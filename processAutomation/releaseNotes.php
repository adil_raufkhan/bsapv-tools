<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

// Define globals here
$configs['pathToUser'] = "/Users/jaina/";
$configs['wikiUrl'] = "http://confluence.beachbodyondemand.local";
$configs['jiraUrl'] = "http://jira.beachbodyondemand.local";
$configs['date'] = date('F d, Y');

// Get the version from request url
//preg_match("/version=(\d+)&/", $_SERVER['REQUEST_URI'], $output_array);
$versionId = $output_array[1];

// Get the cards for release version and the formatted html message.
$cards = getCards($versionId);
$message = getHtmlMessage($cards);

// Fire the actions
$res = doUpdateWiki($message);
//doEmail($message);
exit;


function doUpdateWiki($message) {
    global $configs;
    $title = "Release Notes: ".$configs['date'];
    $parentJiraId = "3081568";// Hardcoded for now, if changed find out using http://confluence.beachbodyondemand.local/rest/api/content?title=Release+NotesAutomated+Release+Notes&spaceKey=TECH&expand=id

    global $configs;
    $content = array("title" => $title,
        "type" => "page",
        "space" => array("key" => "TECH"),
        "ancestors" => array(array("type" => "page", "id"=>intval($parentJiraId))),
        "body" => array("storage" => array("value" => ($message),"representation" =>"storage"))
    );

    $jsonStr = json_encode($content);
    $url = " -X POST -H 'Content-Type: application/json' -d' ".$jsonStr." ' {$configs['wikiUrl']}/rest/api/content";
    return callConfluence($url, true);
}

function doEmail($cards) {
        $date = date('F d, Y');

        $headers = "From: jaina@beachbody.com\r\n";
        $headers .= "Reply-To: jaina@beachbody.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        mail('jaina@beachbody.com', "Release Notes - $date", $message, $headers);
}

function getWikiMessage($cards) {
    global $configs;
    $date = date('F d, Y');
    $message = ".. h1. Release Notes - ".$date;
    foreach($cards as $type => $issues) {

        $message .= ".. h2. {$type}";

        foreach($issues as $issue) {
            $message .= "..*[['".$configs['jiraUrl']."/browse/".$issue['key']."'>".$issue['key']."]] -         ".$issue['summary'];
        }

        $message .= "..";
    }
    return $message;
}

function getHtmlMessage($cards) {
    global $configs;
    $date = date('F d, Y');
    //$message = "<html><body>";
    $message = "<h1>Release Notes: Beachbody Digital - ".$date."</h1>";
    foreach($cards as $type => $issues) {

        $message .= "<h2> {$type} </h2><ul>";

        foreach($issues as $issue) {
            $message .= '<li>[<a href="'.$configs['jiraUrl']."/browse/".$issue['key'].'">'.$issue['key']."</a>] -         ".$issue['summary']."</li>";
        }

        $message .= "</ul>";
    }
    //$message .= "</body></html>";
    return $message;
}

function callConfluence($url, $post=false) {
    global $configs;
    $cmd = "curl -H 'Authorization: Basic amFpbmE6amlyYXB3ZDIwMTU' --verbose ".$url;
    var_dump($cmd);
    $response = shell_exec($cmd);
    $response = json_decode($response, true);
    return $response;
}

function getCards($versionId) {


    global $configs;
    $url = $configs['jiraUrl']."/rest/api/latest/search?jql=fixVersion={$versionId}";
    $curl = curl_init();

    $headers = array( "Authorization: Basic amFpbmE6amlyYXB3ZDIwMTU");
    //$headers = array( "Content-Type: application/json");

    curl_setopt($curl, CURLOPT_HEADER, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_URL, $url);
    $result = curl_exec($curl);

    $parts = explode("\r\n\r\n", $result);
    $body = array_pop($parts);

    $rawData = json_decode($body,true);
    $cards = array();
    foreach($rawData['issues'] as $issue) {

            $cards[$issue['fields']['issuetype']['name']][] = array('key' => $issue['key'], 'summary' =>$issue['fields']['summary']);
    }
    return $cards;
}
?>
