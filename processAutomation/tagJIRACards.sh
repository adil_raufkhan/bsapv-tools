#!/bin/bash

# Housekeeping
export version=$(date +"%Y%m%d")
export data1='{"name":"'$version'","project":"BOD"}'
export data2='{"update":{"fixVersions":[{"set":[{"name":"'$version'"}]}]}}'

# Create a new version in JIRA
curl -H "Authorization: Basic amFpbmE6amlyYXB3ZDIwMTU=" -H "Content-Type: application/json" -X POST --data $data1 http://jira.beachbodyondemand.local/rest/api/latest/version

# Get all cards to get tagged
export diff=$(git log --pretty=oneline master...$1 | grep -i "bod")
export cards=()

   while read -r line; do
        printf '%s\n' "$line"
        export ticket=$(echo $line |  sed 's/.*BOD\-\([0-9A-Z][0-9A-Z]*\)\:*.*/\1/p;d')

        export url="http://jira.beachbodyondemand.local/rest/api/latest/issue/BOD-$ticket"
echo $url
# Add fix version
curl -D- -H "Authorization: Basic amFpbmE6amlyYXB3ZDIwMTU=" -H "Content-Type: application/json" -X PUT --data $data2 $url

    done <<< "$diff"
