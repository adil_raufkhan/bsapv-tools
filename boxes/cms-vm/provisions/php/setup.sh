#!/bin/bash

sudo su -c 'rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm'

sudo su -c 'yum --enablerepo=epel,remi -y install php php-pear php-cli php-common php-devel gd php-gd php-mbstring php-mcrypt php-pdo php-soap php-xml php-xmlrpc php-bcmath php-pecl-memcache php-snmp php-mssql php-imap php-pgsql php-redis php-apc'

sudo su -c 'cp /vagrant/provisions/php/php.ini /etc/php.ini'

