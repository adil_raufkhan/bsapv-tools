<?php

if(!function_exists('getuserarray')) {
    function getuserarray()
    {
        if (isset($_GET['debug-user'])) {
            $_GET['debug-user'] = array(
                'LAST_NAME' => 'Piskulic',
                'CUSTOMER_TYPE' => 'REGISTEREDUSER,CLUB',
                'OAM_REMOTE_USER' => 'App@Club2beachbodytest.com',
                'FIRST_NAME' => 'Boban-UAT',
                'GUID' => '123456789',
                'ANALYTICS_OVERRIDE' => '5397547c326970d5002e8b9f51db18031d79b482aec3e70cf0da743f7fd91b7d'
            );
            session_start();
            $_SESSION['debug-user'] = $_GET['debug-user'];
        }
        return isset($_SESSION['debug-user']) && is_array($_SESSION['debug-user']) ?
            $_SESSION['debug-user'] : getallheaders();
    }
}

$configurations['CONST']['LOGIN_REQUIRED_VALUE'] = 'REGISTEREDUSER,UKCLUB,CLUB';
$configurations['CONST']['LOGOUT_URL'] = 'http://' . $_SERVER['SERVER_NAME'] . ':8181';
$configurations['CONST']['LOGIN_SUBMIT_URL'] = 'http://' . $_SERVER['SERVER_NAME'] . ':8181/?debug-user';
$configurations['CONST']['LOGIN_RECEIVE_URL'] = 'http://' . $_SERVER['SERVER_NAME'] . ':8181/?debug-user&redirect_url=http%3A%2F%2F';

$configurations['CONST']['DB_NAME'] = 'digiwp_db';
$configurations['CONST']['DB_NAME2'] = 'digiwp_db';
$configurations['CONST']['DB_USER'] = 'vagrant';
$configurations['CONST']['DB_PASSWORD'] = 'vagrantpass';
$configurations['CONST']['DB_HOST'] = 'localhost';

# Configuration to point to local API server (app-vm)
# $configurations['CONST']['API_SERVER'] = '172.10.10.10:8080';
# $configurations['CONST']['API_SERVER_AUTHORIZATION_TOKEN'] = 'test123';

$configurations['CONST']['API_SERVER'] = 'digiapp01.dev.el.beachbody.com';
$configurations['CONST']['API_SERVER_AUTHORIZATION_TOKEN'] = 'test123';
