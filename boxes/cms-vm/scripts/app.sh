#!/bin/bash

sudo su -c "cp /vagrant/provisions/apache/conf.d/beachbody-cms.conf /etc/httpd/conf.d/"

echo "Restarting Apache"
sudo su -c "service httpd restart"

echo "Enable MySQL Access and Development Permissions"
sudo su -c 'mysql -uroot < /vagrant/scripts/permissions.sql'

echo "Load the BB data"
sudo su -c 'mysql -uroot -e "create database if not exists digiwp_db DEFAULT CHARACTER SET utf8;"'
sudo su -c 'mysql -uroot -D digiwp_db < /vagrant/scripts/wp_db_2015-06-11.sql'

echo "Install local.php configuration"
sudo su -c "cp /vagrant/scripts/environments/local.php /mnt/cms/environments/"

sudo su -c "service httpd restart"

echo 'Done!'
