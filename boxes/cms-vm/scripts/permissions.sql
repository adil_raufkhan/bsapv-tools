
-- Allow localhost
GRANT ALL PRIVILEGES ON *.* TO 'vagrant'@'localhost' IDENTIFIED BY 'vagrantpass' WITH GRANT OPTION;

-- Allow host machine
GRANT ALL PRIVILEGES ON *.* TO 'vagrant'@'172.10.10.1' IDENTIFIED BY 'vagrantpass' WITH GRANT OPTION;

-- Allow APP machine access
GRANT ALL PRIVILEGES ON *.* TO 'vagrant'@'172.10.10.10' IDENTIFIED BY 'vagrantpass' WITH GRANT OPTION;

FLUSH PRIVILEGES;

