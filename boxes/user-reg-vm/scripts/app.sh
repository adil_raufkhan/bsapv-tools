#!/bin/bash

sudo su -c "cp /vagrant/provisions/apache/conf.d/beachbody-user-reg.conf /etc/httpd/conf.d/"

echo "Configure User API"
echo "Install local.php configuration"
sudo su -c "cp /vagrant/scripts/environments/user-api-local.php /mnt/user-api/config/autoload/local.php"

echo "Configure User Registration Site"
echo "Install local.json configuration"
sudo su -c "cp /vagrant/scripts/environments/user-reg-local.json /mnt/user-registration-site/config/local.json"

echo "Restarting Apache"
sudo su -c "service httpd restart"

echo 'Done!'
