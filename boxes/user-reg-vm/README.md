
# VM to dev/test User Registration flow

This VM serves up the `/user-api` and `/user-registration-site` repos on different ports.

Continue to use the `app-vm` and `cms-vm` for the API and Wordpress site.

## Folder structure

All folders should be at the same level in the file system.

```
/app                    # Digital API
/cms                    # Wordpress Site
/user-api               # User registration API
/user-registration-site # User registration site repo
/boxes                  # This repo
```

## Starting the Vagrant box

### User Reg VM
1. Go to `/boxes/user-reg-vm`
2. Execute `vagrant up`
3. Visit `http://localhost:9000` to see the user API.
4. Visit `http://localhost:9001` in a browser to view the registration site.



