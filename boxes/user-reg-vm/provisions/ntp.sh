#!/bin/bash

echo "Setup NTP (automatic Internet-timeclock sync)"

sudo su -c "yum install -y ntp"

sudo su -c "ntpdate pool.ntp.org"

sudo su -c "service ntpd restart"

sudo su -c "chkconfig ntpd on"

echo "Finished NTP setup"

