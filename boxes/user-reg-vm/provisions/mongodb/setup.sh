#!/bin/bash -x

# Copy configuration for repo
sudo su -c 'cp /vagrant/provisions/mongodb/mongodb-org-3.0.repo /etc/yum.repos.d/mongodb-org-3.0.repo'

# After repo is copied we need to run install
echo "Installing MongoDB"
sudo su -c "yum install -y mongodb-org"

echo "Setting up ENV"
sudo su -c "yum provides /usr/sbin/semanage"
sudo su -c "yum -y install policycoreutils-python"

echo "Configure MongoDB"
sudo su -c 'cp /vagrant/provisions/mongodb/mongod.conf /etc/mongod.conf'

echo "Starting MongoDB"
sudo su -c "service mongod start"
sudo su -c "use digicontent_db"

echo "Install mongo shared object"
sudo su -c 'yum install -y --enablerepo=remi,remi-php55 php-pecl-mongo'

sudo su -c 'service httpd start'

echo "Done installing mongodb"
