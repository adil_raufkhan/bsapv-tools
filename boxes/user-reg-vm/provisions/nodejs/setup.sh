#!/bin/bash

# Download and install nodejs
echo "Install Node.js"
if [ ! -f /root/node-v0.12.4.tar.gz ]
then
  sudo su -c "cd ~ && wget --no-clobber http://nodejs.org/dist/v0.12.4/node-v0.12.4.tar.gz"
fi
sudo su -c "cd ~ && tar xvf node-v0.12.4.tar.gz"
sudo su -c "cd ~ && cd node-v0.12.4 && ./configure && make && make install"
echo "Finished Node.js installation"

echo "Setting up symlinks for root user support"
if [ ! -f /usr/bin/node ]
then
  sudo su -c "ln -s /usr/local/bin/node /usr/bin/node"
fi

if [ ! -f /usr/lib/node ]
then
  sudo su -c "ln -s /usr/local/lib/node /usr/lib/node"
fi

if [ ! -f /usr/bin/npm ]
then
  sudo su -c "ln -s /usr/local/bin/npm /usr/bin/npm"
fi

if [ ! -f /usr/bin/node-waf ]
then
  sudo su -c "ln -s /usr/local/bin/node-waf /usr/bin/node-waf"
fi
echo "Finished setting up symlinks"

# Install Node GYP
echo "Install node-gyp"
npm install -g node-gyp -g
echo "Finished node-gyp installation"

echo "Install node-canvas dependencies"
sudo su -c "yum install -y cairo cairo-devel cairomm-devel libjpeg-turbo-devel pango pango-devel pangomm pangomm-devel"
echo "Finished node-canvas dependencies installation"

# Grunt
echo "Install Grunt"
npm install -g grunt
echo "Finished Grunt installation"


