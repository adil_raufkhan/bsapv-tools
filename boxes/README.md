
# Vagrant boxes for development

## Folder structure

All folders should be at the same level in the file system.  The relative paths to the `/app` repo and `/cms` repo are hardcoded in the respective Vagrantfile.

```
/app   # API repository from Gitlab
/cms   # Wordpress repository from Gitlab
/boxes # This repository
```

## Requirements
- Vagrant 1.6+


## Starting the Vagrant box

### APP VM
1. Go to `/boxes/app-vm`
2. Execute `vagrant up`
3. Visit `http://localhost:8080` in a browser to view Apigility admin page.

### CMS VM
1. Go to `/boxes/cms-vm`
2. Execute `vagrant up`
3. Visit `http://localhost:8181` in a browser to view the Wordpress page.

### User Registration VM
1. Go to `/boxes/user-reg-vm`
2. Execute `vagrant up`
3. Visit `http://localhost:9000/` in browser to view Agigility admin page for User API.
4. Visit `http://localhost:9001/` in browser to view standalone User Registration page.

#### Fonts not working?

On Mac, edit the `/etc/hosts` file and add:

    127.0.0.1	local.teambeachbody.com

After editing the hosts file, visit `http://local.teambeachbody.com:8181` to view the Wordpress page.

