#!/bin/bash

echo "Installing MySQL DB"

sudo su -c 'rpm -Uvh http://dl.fedoraproject.org/pub/epel/6/x86_64/e/epel-release-6-5.noarch.rpm'
sudo su -c 'rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm'

sudo su -c 'rpm -Uvh http://dev.mysql.com/get/mysql-community-release-el6-5.noarch.rpm'

sudo su -c 'yum install -y mysql-server'
sudo su -c 'chkconfig mysqld on'
sudo su -c 'chkconfig --list mysqld'

echo "Configure MySQL DB"
sudo su -c "cp /vagrant/provisions/mysql/my.cnf.d/vagrant.cnf /etc/my.cnf.d/vagrant.cnf"

echo 'Install PHP dependency'
sudo su -c 'yum install -y --enablerepo=remi,remi-php55 php-mysql php-pdo'

echo "Restarting MySQL DB"
sudo su -c 'service mysqld restart'

