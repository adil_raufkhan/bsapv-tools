#!/bin/bash

echo "Install memcached"
sudo su -c "yum -y install memcached"

echo "Start memcached"
sudo su -c 'service memcached restart'

echo "Start memcached on reboot"
sudo su -c 'chkconfig memcached on'


