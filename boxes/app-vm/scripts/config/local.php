<?php
return array(
    'doctrine' => array(
        'connection' => array(

            'ProfileDB' => array(
                'params' => array(
                    'driverClass' => 'Doctrine\\DBAL\\Driver\\PDOMySql\\Driver',
                    'host' => '127.0.0.1',
                    'port' => '3306',
                    'user' => 'vagrant',
                    'password' => 'vagrantpass',
                    # 'user' => 'digiadmin',
                    # 'password' => 'm4M2Mm7A',
                    'dbname' => 'digiprofile_db',
                ),
            ),

            'ContentMongoDB' => array(
                'connection' => array(
                    'odm_default' => array(
                        'host'    => '127.0.0.1',
                        'auth'      => false,
                        'user'      => '',
                        'password'  => '',
                        'port'      => '27017',
                        'dbname'    => 'digicontent_db',
                        'options'   => array(),
                    ),
                ),
                'configuration' => array(
                    'odm_default' => array(
                        'metadata_cache'     => 'array',
                        'driver'             => 'odm_default',
                        'generate_proxies'   => true,
                        'proxy_dir'          => 'data/DoctrineMongoODMModule/Proxy',
                        'proxy_namespace'    => 'DoctrineMongoODMModule\Proxy',
                        'generate_hydrators' => true,
                        'hydrator_dir'       => 'data/DoctrineMongoODMModule/Hydrator',
                        'hydrator_namespace' => 'DoctrineMongoODMModule\Hydrator',
                        'default_db'         => 'digicontent_db',
                        'filters'            => array()
                    )
                ),
            ),

        ),
    ),
);
?>
