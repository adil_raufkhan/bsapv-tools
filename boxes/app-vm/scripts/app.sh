#!/bin/bash

sudo su -c "cp /vagrant/provisions/apache/conf.d/beachbody-app.conf /etc/httpd/conf.d/"

echo "Restarting Apache"
sudo su -c "service httpd restart"

echo "Enable MySQL Access and Development Permissions"
sudo su -c 'mysql -uroot < /vagrant/scripts/permissions.sql'

echo "Load the BB data"
sudo su -c 'mysql -uroot -e "create database if not exists digiprofile_db;"'
sudo su -c 'mysql -uroot -D digiprofile_db < /vagrant/scripts/digiprofile_db-2015-06-29.sql'

echo "Restarting MySQL DB"
sudo su -c 'service mysqld restart'

echo "Load MongoDB data"
sudo su -c 'mongorestore /vagrant/scripts/mongodb-data-20150706' 

echo "Install Composer"
sudo su -c "rm -rf /mnt/app/vendor"
sudo su -c "cd /mnt/app && curl -sS https://getcomposer.org/installer | php"

echo "Install composer dependencies"
sudo su -c "cd /mnt/app && php composer.phar install"

echo "Install local.php configuration"
sudo su -c "cp /vagrant/scripts/config/local.php /mnt/app/config/autoload/"

echo "Restart httpd server"
sudo su -c "service httpd restart"

echo 'Done!'
