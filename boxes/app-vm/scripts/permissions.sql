
-- Allow localhost
GRANT ALL PRIVILEGES ON *.* TO 'vagrant'@'localhost' IDENTIFIED BY 'vagrantpass' WITH GRANT OPTION;

-- Allow host machine
GRANT ALL PRIVILEGES ON *.* TO 'vagrant'@'172.10.10.1' IDENTIFIED BY 'vagrantpass' WITH GRANT OPTION;

-- Allow CMS machine access
GRANT ALL PRIVILEGES ON *.* TO 'vagrant'@'172.10.10.20' IDENTIFIED BY 'vagrantpass' WITH GRANT OPTION;

FLUSH PRIVILEGES;

